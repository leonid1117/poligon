<?php

namespace App\Http\Resources;

use App\Dto\UserRegistered;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property UserRegistered $resource
 */
class EmailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'user' => [
                'id' => $this->resource->user->id,
                'name' => $this->resource->user->name,
            ],
            'token' => $this->resource->token,
        ];
    }
}
