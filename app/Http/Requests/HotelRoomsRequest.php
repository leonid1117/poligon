<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HotelRoomsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'floor' => 'required|max:10',
            'number_seats' => 'required|max:10',
            'price_day' => 'required|max:10',
            'images' => 'required|max:255',
            'specifications' => 'required|max:1000',
        ];
    }
}
