<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email|max:255',
            'password' => 'required|max:255',

        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Пользователь с таким email уже зарегистрирован',
        ];
    }
}
