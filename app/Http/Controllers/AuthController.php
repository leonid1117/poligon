<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\EmailResource;
use App\Jobs\ConfirmEmail;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{

    public function register(RegisterRequest $registerRequest)
    {
        $user = new User();
        $user->fill($registerRequest->validated());
        $user->save();

        $token = $user->createToken('ProjectTokenRegister')->plainTextToken;

        $this->dispatch(new ConfirmEmail($user));

        return EmailResource::make(new \App\Dto\UserRegistered($user, $token));

    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->firstOrFail();
        Hash::check($request->password, $user->password);
        $token = $user->createToken('ProjectTokenLogin')->plainTextToken;
        $response = ['token' => $token];
        return response($response);

    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
    }

    public function confirmEmail(Request $request, $token)
    {
        User::whereEmailConfirmToken($token)->firstOrFail()->confirmEmail();
    }
}
