<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryApiController extends Controller
{
    public function category(Request $request)
    {
        $q = $request->get('id');

       //return Category::where('name', 'like', "%$q%")->paginate(null, ['id', 'name as text']);
        return Category::where('parent_id', $q)->get(['id', DB::raw('name as text')]);
    }
}
