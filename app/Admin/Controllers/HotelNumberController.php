<?php

namespace App\Admin\Controllers;

use App\Models\Category;
use App\Models\HotelNumber;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class HotelNumberController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'HotelNumber';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new HotelNumber());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('floor', __('Floor'));
        $grid->column('number_places', __('Number places'));
        $grid->column('price_day', __('Price day'));
        $grid->column('specifications', __('Specifications'));
        $grid->column('category_id', __('Category id'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(HotelNumber::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('name', __('Name'));
        $show->field('floor', __('Floor'));
        $show->field('number_places', __('Number places'));
        $show->field('price_day', __('Price day'));
        $show->field('specifications', __('Specifications'));
        $show->field('category_id', __('Category id'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new HotelNumber());

        $form->text('name', __('Name'));
        $form->number('floor', __('Floor'));
        $form->number('number_places', __('Number places'));
        $form->decimal('price_day', __('Price day'));
        $form->textarea('specifications')->rows(10);
        $form->select('category_id')->options(Category::query()->pluck('name','id'));

        $form->mediaLibrary(HotelNumber::MEDIA_COLLECTION_IMAGE, __('image'));

        return $form;
    }
}
