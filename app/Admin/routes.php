<?php

use Illuminate\Routing\Router;



Admin::routes();

Route::group([
    'prefix' => config('admin.route.prefix'),
    'namespace' => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
    'as' => config('admin.route.prefix') . '.',
], function (Router $router) {
    $router->resource('category', CategoryController::class);
    $router->resource('hotel-number', HotelNumberController::class);
    $router->get('/', 'HomeController@index')->name('home');
});
