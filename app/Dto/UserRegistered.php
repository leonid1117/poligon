<?php

namespace App\Dto;

use App\Models\User;

/**
 * @property User $user
 * @property string $token
 */
class UserRegistered
{
    public User $user;

    public string $token;

    public function __construct(User $user, string $token)
    {
        $this->user = $user;
        $this->token = $token;
    }
}
